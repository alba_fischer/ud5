package Tasca5;

public class Exercici1App {
	
	public static void main(String[] args) {
		/* Declarem 2 variables num�riques. El programa ens indica
		 * quina �s m�s gran de les 2 o si s�n iguals.
		 */
		int var1 = 2,  var2 = 4 ;
		if (var1 > var2) {
			System.out.println("La var1 �s major que la var2");
		}else if(var1 < var2) {
			System.out.println("La var2 �s major que la var1");
		}else {
			System.out.println("La var1 �s igual que la var2");
		}		
	}
}