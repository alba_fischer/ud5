package Tasca5;

public class Exercici2App {
	
	public static void main(String[] args) {
		/* Declarem un string que contigui el meu nom. Mostrem per consola 
		 * un missatge de benvinguda.
		 */
		String nom = "Alba";
		System.out.println("Benvinguda " + nom);
	}
}