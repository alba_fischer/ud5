package Tasca5;

import javax.swing.JOptionPane;

public class Exercici11App {

	public static void main(String[] args) {
		/*Creem una aplicaci� que ens demani un dia de la setmana
		 * i ens diguii si es dia laborable o si no.
		 */
		String dia = JOptionPane.showInputDialog("Introdueix un dia de la setmana");
		switch(dia) {
			case "Dilluns":
				System.out.println("�s un dia laboral");
				break;
			case "Dimarts":
				System.out.println("�s un dia laboral");
				break;
			case "Dimecres":
				System.out.println("�s un dia laboral");
				break;
			case "Dijous":
				System.out.println("�s un dia laboral");
				break;
			case "Divendres":
				System.out.println("�s un dia laboral");
				break;
			case "Dissabte":
				System.out.println("No �s un dia laboral");
				break;
			case "Diumenge":
				System.out.println("No �s un dia laboral");
				break;
			default:
				System.out.println("No has introdu�t un dia correcte");			
		}
	}
}
