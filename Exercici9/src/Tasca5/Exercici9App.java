package Tasca5;

public class Exercici9App {
	
	public static void main(String[] args) {
	//Mostrem els nombres del 1 al 100 (ambd�s inclosos) divisibles entre 2 i 3
		for (int num = 1; num<=100; num++) {	
			if ((num % 2 == 0 )&&(num % 3 == 0)) {
				System.out.println(num); // S'imprimeixen nom�s els nombres que es poden dividir per 2 i per 3
				/* En el cas de qu� nom�s siguin divisibles per 2 (i per 3 no) o per 3 (i per 2 no) no s'imprimeixen.
				 * Si vulgu�ssim imprimir-los hauriem d'usar or (||) i no and.
				 */				
			}
		}
	}
}