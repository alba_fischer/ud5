package Tasca5;

import javax.swing.JOptionPane;

public class Exercici6App {

	public static void main(String[] args) {
		//Llegim un nombre per teclat i calculem el preu final amb 21% d'IVA
		
		final double IVA = 0.21;
		String preu_text = JOptionPane.showInputDialog("Introdueix el preu del prodcte");
		
		double preu = Double.parseDouble(preu_text);
		System.out.println("El producte t� un preu final de " + (preu+preu*IVA));
	}
}
