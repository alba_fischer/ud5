package Tasca5;

import javax.swing.JOptionPane;

public class CalculadoraInversaApp {

	public static void main(String[] args) {
		/* Creem l'aplicaci� "CalculadoraInversa" que ens demana dos operadors i un signe aritm�tic
		 * Segons el qual realitzar� l'operci� corresponent.
		 */
						
		String num1_text = JOptionPane.showInputDialog("Introdueix el primer operant");
		int num1 = Integer.parseInt(num1_text);
		String num2_text = JOptionPane.showInputDialog("Introdueix el segon operant");
		int num2 = Integer.parseInt(num2_text);
		
		String signe = JOptionPane.showInputDialog("Introdueix un signe aritm�tic");
		
		switch(signe) {
			case "+":
				JOptionPane.showMessageDialog(null, "Suma " + num1 + " + " + num2 + " = " + (num1+num2));
				break;
			case "-":
				JOptionPane.showMessageDialog(null, "Resta " + num1 + " - " + num2 + " = " + (num1-num2));
				break;
			case "*":
				JOptionPane.showMessageDialog(null, "Multiplicaci� " + num1 + " * " + num2 + " = " + (num1*num2));
				break;
			case "/":
				JOptionPane.showMessageDialog(null, "Divisi� " + num1 + " / " + num2 + " = " + (num1/num2));
				break;
			case "^":
				JOptionPane.showMessageDialog(null, "Pot�ncia " + num1 + " ^ " + num2 + " = " + Math.pow(num1, num2));
				break;
			case "%":
				JOptionPane.showMessageDialog(null, "M�dul (residu) " + num1 + " % " + num2 + " = " + (num1%num2));
				break;
			default:
				System.out.println("Aquest signe aritm�tic no est� disponible");			
		}
	}
}
