package Tasca5;

import javax.swing.JOptionPane;

public class Exercici5App {
	public static void main(String[] args) {
		//Llegim un nombre per teclat i indiquem si �s divisible per 2 o no
		String num_text = JOptionPane.showInputDialog("Introdueix un nombre");
		int num = Integer.parseInt(num_text); // Suposem que el nombre que ens demana l'exercici �s un enter
		if (num % 2 == 0) {
			System.out.println("El nombre �s divisible per 2.");
		}else {System.out.println("El nombre no �s divisible per 2.");
		}
	}
}