package Tasca5;

import javax.swing.JOptionPane;

public class Exercici4App {
	public static void main(String[] args) {
		// Calculem l'�rea d'un cercle pi*R^2
		
		String radi_text = JOptionPane.showInputDialog("Introdueix el radi");
		double radi = Double.parseDouble(radi_text);
		double area;
		area = Math.PI * Math.pow(radi, 2);
		System.out.println("�rea = " + area);
		
	}
}
