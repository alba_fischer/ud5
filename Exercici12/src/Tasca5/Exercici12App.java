package Tasca5;

import javax.swing.JOptionPane;

public class Exercici12App {

	public static void main(String[] args) {
		/* Creem una aplicaci� que et demane introdu�r una contrasenya.
		 * Si encertes, et dona l'enhorabona. Tens 3 intents.
		 */
		String contra = "1234";
		int i = 1;
				
		do {
			String contra1 = JOptionPane.showInputDialog("Introdueix la contrasenya");
			if (contra1.contentEquals(contra)) {
				JOptionPane.showMessageDialog(null, "Enhorabona!");
				break;
			}
			i++;
		}while(i<=3);
	}
}
