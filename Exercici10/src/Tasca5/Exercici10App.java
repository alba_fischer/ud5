package Tasca5;

import javax.swing.JOptionPane;

public class Exercici10App {
	
	public static void main(String[] args) {
		/*Fem una aplicaci� que ens demani el nombre de ventes i el valor de cada venta, i ens tragui per
		 * consola la suma total de les ventes.
		 */
				
		String venta_text = JOptionPane.showInputDialog("Introdueix el nombre de ventes");		
		int venta = Integer.parseInt(venta_text), i = 1;
		double venta1, venta_tot = 0;
	
		do { String venta1_text = JOptionPane.showInputDialog("Introdueix el valor de la venta");
			venta1 = Double.parseDouble(venta1_text);
			venta_tot = venta_tot + venta1;
			i++ ; 
		} while(i <= venta) ;
		System.out.println("La suma de les ventes �s: "+ venta_tot);
	}
}
